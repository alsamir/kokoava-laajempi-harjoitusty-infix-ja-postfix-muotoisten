#include "tree.h"

/// node jotta infix/postfix lausekkeen laskusuoritukset osataan suorittaa oikeassa j�rjestyksess�
Tree::node* Tree::createNode( string* data ){
  node* n = (node*)malloc(sizeof(node));
  n -> left = NULL;///Oletuksena nulliarvot puun vasemmille ja oikeille haaroille
  n -> right = NULL;
  n -> parent = NULL;///Nulli my�s parentiks.
  n -> data = data;
  return n;
}

/// Puumuotoinen rakenne laskutoimituksille.. Puulla on root-node, jotta tiedet��n ensimm�inen suoritettava operaatio
Tree::Tree( void ){
  this -> root = NULL;
  this -> setInput();
}

/// Funktio k�ytt�j�sy�tteen vastaanottamiseksi
void Tree::setInput(){
  getline( cin, this -> input );
  while( this -> input.find_first_not_of( " ()+-*/1234567890" ) != string::npos ){
    cout << endl << "Antamasi lauseke oli v��rin.\nLauseke saa sis�lt�� vain v�lily�ntej�, numeroita, operaattoreita '+-*/'\nsek� sulkuja '()'.\nAnna kelvollinen lauseke:" << endl;
    getline( cin, this -> input );
  }
}

/// Luo annetusta lausekkeesta puun
void Tree::createTree( void ){ //Tekee annetusta lausekkeesta lausekepuun
  string input = this -> getInput();
  string* v;
  string value = "";
  bool flag = true; //Apumuuttuja while-looppia varten
  if( input.find_first_of( " " ) == string::npos ){ //Jos lausekkeessa on v�lily�ntej�, oletetaan se postfixiks
    input = this -> toPostfix( input ); //Muutoin tehd��n konversaatio postfixiks
  }
  node* par = NULL;
  node* n = NULL;
  int i = input.length() - 1;
  while( flag ){ //Loopataan kunnes puu on valmis. luetaan lauseke lopusta alkuun, jotta saadaan oikea laskuj�rjestys
    if( this -> isOperand( input[i] ) ){ //jos operaattori
      value = input[i]; //otetaan operaattori talteen
      v = new string( value ); //Asetetaan se string pointteriin
      n = this -> createNode( v ); //Luodaan node, jonka data-arvoksi tulee operaattori
      if( this -> root == NULL ){ //Mik�li rootti alkiota ei oo asetettu, tehd��n se t�st�
        this -> root = n;
        par = n; //Asetetaan parentiksi nykyinen alkio
      }else if( par -> right == NULL ){ //Oletusarvoisesti l�hdet��n lis��m��n puun oikeaan haaraan
        n -> parent = par;
        par -> right = n;
        par = n;
      }else if( par -> left == NULL ){ //Mik�li oikea haara on jo t�ynn�
        n -> parent = par;
        par -> left = n;
        par = n;
      }else{
        while( par -> right && par -> left ){
          par = par -> parent;
        }
        if( par -> right == NULL ){
          n -> parent = par;
          par -> right = n;
          par = n;
        }else{
          n -> parent = par;
          par -> left = n;
          par = n;
        }
      }
      value = ""; //Laitetaan value tyhj�ksi
    }else if( isdigit( input[i] ) ){// jos numero
      value += input[i]; //Lis�t��n numero valueen, ja tarkistetaan tuleeko niit� lis��
      if( i != 0 ){ //jos i on nolla, ollaan ensimm�isess� merkiss�, eik� aiempia merkkej� voi olla
        i--;
      }else{
        input = "s";
      }
    }else if( value != "" ){ //Jos ollaan asetettu value, eik� enemp�� numeroita
      value = string( value.rbegin(), value.rend() ); //K��nnett��n luku (se on luettu per�st� alkuun)
      v = new string( value );
      if( par -> right == NULL ){ //Jos alkion oikee haara on tyhj�, lis�t��n siihen uusi alkio
        n = this -> createNode( v );
        n -> parent = par;
        par -> right = n;
      }else if( par -> left == NULL ){ //Muutoin asetetaan se vasempaan haaraan
        n = this -> createNode( v );
        n -> parent = par;
        par -> left = n;
        par = par -> parent;
      }else{
        while( par -> right && par -> left ){
          par = par -> parent;
        }
        if( par -> right == NULL ){
          n = this -> createNode( v );
          n -> parent = par;
          par -> right = n;
        }else{
          n = this -> createNode( v );
          n -> parent = par;
          par -> left = n;
          par = par -> parent;
        }
      }
      if( input.length() > 1 ){ //Jos inputti on pidempi kuin yhden merkin, poistetaan siit� �skett�in lis�tty luku
        input = input.substr( 0, input.length() - value.length() + 1 );
      }
      value = "";
    }
    if( value == "" ){ //Ja fiksataan sitte ylim��r�set merkit pois (mm v�lily�nnit)
      if( input.length() > 1 ){
        i = input.length() - 2;
        input = input.substr( 0, i );
        i = input.length() - 1;
      }else if( input != "s" ){
        i = 0;
      }else{ //Jos inputti on s, kertoo se ett� puu on valmis
        flag = false;
      }
    }
  }
}

/// Funktio jolla lausekepuu voidaan tulostaa k�ytt�j�lle niin m��ritelt�ess�
void Tree::printTree( node* n ){ //Printtaa valmiin lausekepuun.
  if( n -> left || n -> right ){
    cout << "(" << *n -> data << ") => (" << *n -> left -> data << ") (" << *n -> right -> data << ")" << endl;
    if( n -> left ){
      this -> printTree( n -> left );
    }
    if( n -> right ){
      this -> printTree( n -> right );
    }
  }
}

/// Laskee lausekkeen lopullisen arvon puun perusteella
int Tree::countTree( node* n ){
  int answer = 0;
  node* left = n -> left;
  node* right = n -> right;
  string leftdata = *left -> data;
  string rightdata = *right -> data;
  string data = *n -> data;
  if( this -> isOperand( rightdata[0] ) && this -> isOperand( leftdata[0] ) ){
    if( data[0] == '*' ){
      answer = this -> countTree( left ) * this -> countTree( right );
    }else if( data[0] == '/' ){
      answer = this -> countTree( left ) / this -> countTree( right );
    }else if( data[0] == '+' ){
      answer = this -> countTree( left ) + this -> countTree( right );
    }else if( data[0] == '-' ){
      answer = this -> countTree( left ) - this -> countTree( right );
    }
  }else{
    if( this -> isOperand( rightdata[0] ) ){
      if( data[0] == '*' ){
        answer = atoi( leftdata.c_str() )  * this -> countTree( right );
      }else if( data[0] == '/' ){
        answer = atoi( leftdata.c_str() )  / this -> countTree( right );
      }else if( data[0] == '+' ){
        answer = atoi( leftdata.c_str() )  + this -> countTree( right );
      }else if( data[0] == '-' ){
        answer = atoi( leftdata.c_str() )  - this -> countTree( right );
      }
    }else if( this -> isOperand( leftdata[0] ) ){
      if( data[0] == '*' ){
        answer = this -> countTree( left ) * atoi( rightdata.c_str() );
      }else if( data[0] == '/' ){
        answer = this -> countTree( left ) / atoi( rightdata.c_str() );
      }else if( data[0] == '+' ){
        answer = this -> countTree( left ) + atoi( rightdata.c_str() );
      }else if( data[0] == '-' ){
        answer = this -> countTree( left ) - atoi( rightdata.c_str() );
      }
    }else{
      if( data[0] == '*' ){
        answer = atoi( leftdata.c_str() ) * atoi( rightdata.c_str() );
      }else if( data[0] == '/' ){
        answer = atoi( leftdata.c_str() ) / atoi( rightdata.c_str() );
      }else if( data[0] == '+' ){
        answer = atoi( leftdata.c_str() ) + atoi( rightdata.c_str() );
      }else if( data[0] == '-' ){
        answer = atoi( leftdata.c_str() ) - atoi( rightdata.c_str() );
      }
    }
  }

  return answer;
}

/// Funktio jolla voidaan tarkistaa onko merkki operaattori
bool Tree::isOperand( char i ){ //Palauttaa truen jos kyseinen merkki on operaattori
  if( i == '*' || i == '/' || i =='+' || i == '-' ){
    return true;
  }else{
    return false;
  }
}

/// Muuttaa infix-lausekkeen postfix muotoiseksi, jotta lausekkeet voidaan laskea samoilla funktioilla
string Tree::toPostfix( string input ){
  //Tyhj� pino ja tuleva postfixmuotoinen lauseke
	stack<char> opStack;
	stringstream postFixString;
	//Loopataan stringi
  int i = 0;
	while( input[i] != '\0'){
		//Jos numero
		//Jos operaattori, se lis�t��n pinoon joka pysyy j�rjestyksess� laskus��nt�jen mukaan
		if ( isdigit( input[i] ) ){
      if( isdigit( input[i+1] ) ){
        postFixString << input[i];
      }else{
        postFixString << input[i] << ' ';
      }
    }else if(this -> isOperand( input[i] ) ){
			while( !opStack.empty() && opStack.top() != '(' && this -> compareOperators( opStack.top(), input[i] ) <= 0 ){
				postFixString << opStack.top() << ' ';
				opStack.pop();
			}
			opStack.push( input[i] );
		}
		//Pushataan kaikki sulkeet pinoon
		//Kun tullaan loppu sulkeeseen, popataan siihen menness� tulleet operaattorit lausekkeeseen
		else if( input[i] == '('){
      opStack.push( input[i] );
    }
		else if( input[i] == ')' ){
			while( !opStack.empty() ){
				if( opStack.top() == '(' ){
          opStack.pop();
          break;
        }
				postFixString << opStack.top() << ' ';
				opStack.pop();
			}
		}
		//Kasvatetaan apumuuttujaa i seuraavaan merkkiin
		i++;
  }
  //Lis�t��n pinosta loput operaattorit stringiin, jos viel� j�ljell�
  i = 0;
  while (!opStack.empty()) {
    if( i == 0 ){ //jos ollaan ekaa kertaa, ei lis�t� alkuun v�li�, kus se jo l�ytyy
		  postFixString << opStack.top();
      i = 1;
    }else{
      postFixString << ' ' << opStack.top();
    }
		opStack.pop();
	}
  cout << "=" << postFixString.str() << endl;
  return postFixString.str();
}

/// Vertailee operaattoreita, ett� kumman suoritusj�rjestys on ensin
int Tree::compareOperators( char op1, char op2 ){
	if( (op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-') ){
    return -1;
  }
	else if( (op1 == '+' || op1 == '-') && (op2 == '*' || op2 == '/') ){
    return 1;
  }
	return 0;
}
