#ifndef _DLL_MYMATH_H
#define _DLL_MYMATH_H
#include <iostream>
#include <array>
 
 
namespace mymath{
	__declspec(dllexport) double sin(double rad, int type);
	__declspec(dllexport) double cos(double rad, int type);
	__declspec(dllexport) double tan(double rad, int type);
	__declspec(dllexport) double cotan(double rad, int type);
}
 
#endif