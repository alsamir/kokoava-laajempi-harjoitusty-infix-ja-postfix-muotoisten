/**
*	Kurssi/harjoitus: Ohjelmistotuotteen yll�pito | Laajempi kokoava harkka
*	Tehnyt: Pasi Laakso, Sami Ahonen, Samir Al Kurdi
*	Pvm: 29.4.2014
*	Mit�: Ohjelma sis�lt�� komentorivipohjaisen sovelluksen. Sovellukselle annetaan
*	komentoriviparametreina haluttu toiminto ja mahdollisesti lis�optio (kuten degree/radian).
*	Sovellukseen on toteutettu dll-kirjastona itsetehdyt trigonometriset funktiot sin,cos ja tan.
*/

#include <stdio.h>
#include <Windows.h>
#include "mymath.h"
#include "tree.h"

/// checkregistry on simppeli funktio jonka avulla tarkistetaan ett� rekisterist� l�ytyy ohjelman k�ytt�m�t arvot
int checkregistry(){
	HKEY hKey;
	int trigtype = 0;
	LPCTSTR sKey = TEXT("Software\\trigtype");//Tsekataas l�ytyyks rekisterist� jonkin sortin arvoa radians/degrees
	LONG ret = RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_QUERY_VALUE, &hKey );
	if( ret == ERROR_SUCCESS ){//Jos ei oo ees avainta ni ei menn� iffiin
		DWORD lpcbData;
		BYTE Data[sizeof(DWORD)]; 
		DWORD type = REG_DWORD;
		DWORD val;
		ret = RegQueryValueEx(hKey,TEXT("radians"),0,&type,Data,&lpcbData);
		if (ret == ERROR_SUCCESS){//Jos onnistui
			memcpy(&val,Data,lpcbData);
			trigtype = val;
		}else{//Jos jostain syyst� ei l�ytyny radianseille asetettua arvoa
			RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_SET_VALUE, &hKey );
			val = 0;//Asetetaan nolla
			type = REG_DWORD;
			ret = RegSetValueEx(hKey,TEXT("radians"),0,type,(LPBYTE)&val,sizeof(DWORD));
		}
		RegCloseKey(hKey);
	}else if( ret == ERROR_NO_MATCH || ret == ERROR_FILE_NOT_FOUND ){//Jos ei l�ytyny avainta
		//Luodaan se
		long j = RegCreateKeyEx(HKEY_CURRENT_USER, sKey, 0L, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL );
		
		if (j == ERROR_SUCCESS ){//Jos onnistu ni asetetaan oletus-arvo radiansille
			RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_SET_VALUE, &hKey );
			DWORD type = REG_DWORD;
			DWORD val = 0;//0 k�ytt�� radianeita
			ret = RegSetValueEx(hKey,TEXT("radians"),0,type,(LPBYTE)&val,sizeof(DWORD));//Asetetaan se avain
			RegCloseKey(hKey);
		}
	}else{//Jos joku muu virhe ku avaimen puute
		printf("Ei vaan onnistu prkl");
	}
	return trigtype;
}

/// Funktiolla writeregistry voidaan kirjoittaa haluttu arvo rekisteriin avaimelle trigtype
int writeregistry(int value){
	HKEY hKey;
	int trigtype = 0;
	LPCTSTR sKey = TEXT("Software\\trigtype");//Tsekataas l�ytyyks rekisterist� jonkin sortin arvoa radians/degrees
	LONG ret = RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_QUERY_VALUE, &hKey );
	if( ret == ERROR_SUCCESS ){//Jos ei oo ees avainta ni ei menn� iffiin
		DWORD lpcbData;
		BYTE Data[sizeof(DWORD)]; 
		DWORD type = REG_DWORD;
		DWORD val;
		RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_SET_VALUE, &hKey );
		val = value;//0/1
		type = REG_DWORD;
		ret = RegSetValueEx(hKey,TEXT("radians"),0,type,(LPBYTE)&val,sizeof(DWORD));
		RegCloseKey(hKey);
		trigtype = value;
	}else if( ret == ERROR_NO_MATCH || ret == ERROR_FILE_NOT_FOUND ){//Jos ei l�ytyny avainta
		//Luodaan se
		long j = RegCreateKeyEx(HKEY_CURRENT_USER, sKey, 0L, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL );
		
		if (j == ERROR_SUCCESS ){//Jos onnistu ni asetetaan arvo radiansille
			RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_SET_VALUE, &hKey );
			DWORD type = REG_DWORD;
			DWORD val = value;//0/1
			ret = RegSetValueEx(hKey,TEXT("radians"),0,type,(LPBYTE)&val,sizeof(DWORD));//Asetetaan se avain
			RegCloseKey(hKey);
			trigtype = value;
		}
	}else{//Jos joku muu virhe ku avaimen puute
		printf("Ei vaan onnistu prkl");
	}
	return trigtype;
}

/// deleteregkeys poistaa ohjelman asettamat rekisterimerkinn�t.
bool deleteregkeys(){
	HKEY hKey;
	LPCTSTR sKey = TEXT("Software\\trigtype");//Tsekataas l�ytyyks rekisterist� jonkin sortin arvoa radians/degrees
	LONG ret = RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_QUERY_VALUE, &hKey );
	if( ret == ERROR_SUCCESS ){//Jos ei oo ees avainta ni ei menn� iffiin
		DWORD lpcbData;
		BYTE Data[sizeof(DWORD)]; 
		DWORD type = REG_DWORD;
		DWORD val;
		RegOpenKeyEx(HKEY_CURRENT_USER,sKey,0,KEY_SET_VALUE, &hKey );
		type = REG_DWORD;
		ret = RegDeleteValue(hKey,TEXT("radians"));
		RegCloseKey(hKey);
		RegDeleteKey(HKEY_CURRENT_USER, sKey);
		return true;
	}else{//Jos joku muu virhe ku avaimen puute
		printf("Ei vaan onnistu prkl");
		return false;
	}
}

/// main-funktio toimii ohjelman k�ytt�liittym�n virassa. Siin� hallitaan komentoriviparametrit ja kutsutaan metodit.
int main(int argc,char *argv[]){
	setlocale( LC_ALL, "finnish" );
	using namespace mymath;
	int trigtype = checkregistry(); //0 radians, 1 degrees
	if( argc < 2 ){//Jos ei ole annettu toimintoparametreja
		cout << "Tulostaaksesi ohjelmassa k�ytett�v�t komennot, sy�t� -h ohjelman nimen per��n esim 'ohjTuotYlPiLaaja.exe -h'." << endl;
	}else{//Jos ohjelman nimen lis�ksi muitakin parametreja
		string param = argv[1];
		if( param.substr(0,1) == "-" ){//Tsekataan onko parametrin eka merkki viiva (vivut)
			if( param == "-h" ){//Jos helppi
				cout << "-h			Tulostaa ohjeita. Muunmuassa k�ytett�viss� olevat komennot." << endl << endl;
				cout << "-delreg	Poistaa rekisterimerkinn�t." << endl << endl;
				cout << "-infix, -postfix	Voit sy�tt�� lausekkeen infix- tai postfix-muodossa. Komento k�ynnist�� ohjelman, jonka j�lkeen lauseke sy�tet��n" << endl << endl;
				cout << "-sin			Voit laskea sinin. Pyyt�� laskettavan luvun erikseen. Oletuksena radiaanit, mutta antamalla kolmanneksi parametriksi '-d' tai '-r' voidaan korvata edellinen asetus." << endl << endl;
				cout << "-cos			Voit laskea cosinin. Pyyt�� laskettavan luvun erikseen. Oletuksena radiaanit, mutta antamalla kolmanneksi parametriksi '-d' tai '-r' voidaan korvata edellinen asetus." << endl << endl;
				cout << "-tan			Voit laskea tangentin. Pyyt�� laskettavan luvun erikseen. Oletuksena radiaanit, mutta antamalla kolmanneksi parametriksi '-d' tai '-r' voidaan korvata edellinen asetus." << endl << endl;
			}else if( param == "-delreg" ){
				if( deleteregkeys() ){
					cout << "Poistaminen rekisterist� onnistui.";
				}else{
					cout << "Poistaminen rekisterist� ep�onnistui.";
				}
			}else if( param == "-infix" || param == "-postfix" ){//Jos halutaan laskea infix tai postfix-muotonen lauseke
				bool printtree = false;
				if( argc > 2 ){
					string param2 = argv[2];
					if( param2 == "-h" ){
						cout << "-printtree		Kolmas parametri tulostaa lausekepuun sy�tetyst� lausekkeesta." << endl << endl;
						return 0;
					}else if( param2 == "-printtree" ){
						printtree = true;
					}else{
						cout << "Viimeinen parametri oli virheellinen, joten sit� ei huomioida" << endl;
					}
				}
				string input;
				cout << "Anna infix tai outfix-muotoinen lauseke:" << endl;
				Tree tree;
				tree.createTree();
				cout << endl << endl;
				Tree::node* n = tree.root;
				if( printtree ){
					tree.printTree( n );
				}
				cout << "Vastaus antamallesi lausekkeelle on: " << tree.countTree( n ) << endl;
			}else if( param == "-sin" ){//Jos halutaan laskea sin
				if( argc > 2 ){
					string param2 = argv[2];
					if( param2 == "-h" ){
						cout << "-d		Lis��m�ll� t�m�n parametrin, voit sy�tt�� arvot asteina." << endl << endl;
						cout << "-r		Lis��m�ll� t�m�n parametrin, voit sy�tt�� arvot radiaaneina.." << endl << endl;
						return 0;
					}else if( param2 == "-r" ){
						trigtype = writeregistry(0);
					}else if( param2 == "-d" ){
						trigtype = writeregistry(1);
					}else{
						cout << "Viimeinen parametri oli virheellinen, joten sit� ei huomioida" << endl;
					}
				}
				string tyyppi = "radiaaneina";
				if( trigtype == 1 ){
					tyyppi = "asteina";
				}
				cout << "Sy�t� kulma " << tyyppi << ":" << endl;
				double rad;
				while (1) {
					cin >> rad;
					if(cin.good()){
						break;
					}else{
						cout << "Virheellinen arvo.\n" << "Sy�t� kulma " << tyyppi << ":" << endl;
						cin.clear();
						cin.ignore(100000, '\n');
					}
				}
				printf("Sini antamallesi kulmalle on %f\n", mymath::sin(rad, trigtype));
			}else if( param == "-cos" ){
				if( argc > 2 ){
					string param2 = argv[2];
					if( param2 == "-h" ){
						cout << "-d		Lis��m�ll� t�m�n parametrin, voit sy�tt�� arvot asteina." << endl << endl;
						cout << "-r		Lis��m�ll� t�m�n parametrin, voit sy�tt�� arvot radiaaneina.." << endl << endl;
						return 0;
					}else if( param2 == "-r" ){
						trigtype = writeregistry(0);
					}else if( param2 == "-d" ){
						trigtype = writeregistry(1);
					}else{
						cout << "Viimeinen parametri oli virheellinen, joten sit� ei huomioida" << endl;
					}
				}
				string tyyppi = "radiaaneina";
				if( trigtype == 1 ){
					tyyppi = "asteina";
				}
				cout << "Sy�t� kulma " << tyyppi << ":" << endl;
				double rad;
				while (1) {
					cin >> rad;
					if(cin.good()){
						break;
					}else{
						cout << "Virheellinen arvo.\n" << "Sy�t� kulma " << tyyppi << ":" << endl;
						cin.clear();
						cin.ignore(100000, '\n');
					}
				}
				printf("Cosini antamallesi kulmalle on %f\n", mymath::cos(rad, trigtype));
			}else if( param == "-tan" ){
				if( argc > 2 ){
					string param2 = argv[2];
					if( param2 == "-h" ){
						cout << "-d		Lis��m�ll� t�m�n parametrin, voit sy�tt�� arvot asteina." << endl << endl;
						cout << "-r		Lis��m�ll� t�m�n parametrin, voit sy�tt�� arvot radiaaneina.." << endl << endl;
						return 0;
					}else if( param2 == "-r" ){
						trigtype = writeregistry(0);
					}else if( param2 == "-d" ){
						trigtype = writeregistry(1);
					}else{
						cout << "Viimeinen parametri oli virheellinen, joten sit� ei huomioida" << endl;
					}
				}
				string tyyppi = "radiaaneina";
				if( trigtype == 1 ){
					tyyppi = "asteina";
				}
				cout << "Sy�t� kulma " << tyyppi << ":" << endl;
				double rad;
				while (1) {
					cin >> rad;
					if(cin.good()){
						break;
					}else{
						cout << "Virheellinen arvo.\n" << "Sy�t� kulma " << tyyppi << ":" << endl;
						cin.clear();
						cin.ignore(100000, '\n');
					}
				}
				printf("Tangentti antamallesi kulmalle on %f\n", mymath::tan(rad, trigtype));
			}else{//Jos ei mik��n ohjelmaan luotu toiminto, annetaan virheilmotus
				cout << "Antamasi komento oli virheellinen. Tulostaaksesi ohjelmassa k�ytett�v�t komennot, sy�t� -h ohjelman nimen per��n esim 'ohjTuotYlPiLaaja.exe -h'." << endl;
			}
		}else{
			cout << "Virheellinen komentoriviparametri. Parametrin edess� tulee olla aina '-'. Sy�t� uudestaan. Ohjelman komennot saat tulostettua sy�tt�m�ll� parametriksi '-h'." << endl;
		}
	}
    return 0;
}
