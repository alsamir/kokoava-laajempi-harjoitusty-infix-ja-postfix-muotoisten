#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <ctype.h>
#include <stack>
#include <cctype>

using namespace std;
//! Tree on luokka lausekepuuta varten. 
/*!
  Lausekepuun avulla saadaan tulostettua puu, ja toisaalta laskettua helposti lausekkeet matikkas��nt�jen 
  mukasessa j�rjestyksess�
*/
class Tree{/// Luokka puu, jonka avulla saadaan suoritettua sy�tetty matemaattinen lauseke laskus��nt�jen mukaisessa j�rjestyksess�
public:
  typedef struct NODE{///Node auttaa linkitt�m��n puuhun oikeat laskutoimitukset kesken��n
    struct NODE* left;///Vasemmalle tuleva haara
    struct NODE* right;///Oikeelle tuleva haara
    struct NODE* parent;///Kertoo viel� mink� laskutoimituksen alla haara on
    string* data;///Ja sitte se ite laskutoimitus
  } node;///M��ritell��n oletukseks structiks node
  node* root;///Root-node kertoo ns. ensimm�isen suoritettavan laskutoimituksen
  string input;///Input-variaabeliin otetaan post- tai infixmuotoinen lauseke, josta parsitaan lausekepuu ja suoritetaan laskut
  Tree( void );///konstruktori
  node* createNode( string* data );///Luodaan node t�ll�
  void setInput( void );///T�� funktioo kysyy k�ytt�j�lt� lausekkeen ja asettaa sen puulle
  string getInput( void ) {return this -> input;};///Palauttaa puulla olevan lausekkeen
  void createTree( void );///Luo puun inputin perusteella, siten ett� laskutoimitukset suoritetaan oikeassa j�rjestyksess�
  void printTree( node* n );///Tulostaa lausekepuun
  int countTree( node* n );///Palauttaa lausekkeen laskutoimitukselle vastauksen
  string toPostfix( string infix );///Muuttaa infix-muotosen lausekkeen postfixiks.. Siks ettei tarvihte teh� laskujuttuja kahdesti
  int compareOperators( char op1, char op2 );///Vertaa kumpi laskutoimitus suoritetaan ensin
  bool isOperand( char i );///Tsekitt�� onko kysytty merkki operaattori vai ei
};