#include <iostream>
#include <math.h>
#include <iomanip>
#include "mymath.h"
 
#define DLL_EXPORT
 
namespace mymath{
	double pi = 3.14159265358979;
	/// Kirjaston sis�ll� k�ytett�v� funktio kertoma
	double kertoma(double number){
		double factorial=1;
		for(int i=number;i>0;i--)
		factorial=factorial*i;
		return factorial;
	}

	/// Kirjaston sis�ll� k�ytett�v� funktio joka muuttaa asteet radiaaneiksi
	double degtorad(double deg){
		double rad = (deg*(pi/180));
		return rad;
	}

	/// Kirjaston tarjoama funktio sinin laskemiseksi
	__declspec(dllexport) double sin(double rad, int type){
		if( type == 1 ){
			rad = degtorad(rad);
		}
		double sine = rad;
		bool isNeg;
		isNeg = true;
		for (double i=3;i<=30;i+=2)
		{
			if(isNeg)
			{
				sine -= pow(rad,i)/kertoma(i);
				isNeg = false;
			}
			else
			{
				sine += pow(rad,i)/kertoma(i);
				isNeg = true;
			}
		}
		return sine;
	}
	
	/// Kirjaston tarjoama funktio cosinin laskemiseksi
	__declspec(dllexport) double cos(double rad, int type){
		if( type == 1 ){
			rad = degtorad(rad);
		}
		rad = (pi/2)-rad;
		double res = sin(rad, 0);
		return res;
	}
	
	/// Kirjaston tarjoama funktio tangentin laskemiseksi
	__declspec(dllexport) double tan(double rad, int type){
		if( type == 1 ){
			rad = degtorad(rad);
		}
		double result = sin(rad,0)/cos(rad,0);
		return result;
	}
}