LIBS = advapi32.lib

main.exe : main.obj tree.obj mymath.obj
	link main.obj tree.obj mymath.obj $(LIBS)
	
main.obj : main.cpp tree.h mymath.h
	cl -EHsc -c main.cpp
	
tree.obj : tree.cpp tree.h
	cl -EHsc -c tree.cpp
	
mymath.obj : mymath.cpp mymath.h
	cl -EHsc -c mymath.cpp
	
clean :
	del main.exe main.obj tree.obj mymath.obj main.lib main.exp
	
all : clean main.exe
	