K�ytt�tarkoitus:
Ohjelmisto on tarkoitettu k�ytett�v�ksi infix ja postfix muotoisten lausekkeiden laskemiseen.
Lis�ksi siihen on toteutettu trigonometriset funktiot sin, cos ja tan.
Trigonometrisia funktioita ei voi kuitenkaan t�ss� versiossa viel� k�ytt�� infix ja postfix muotoisissa lausekkeissa.
Ennen edell�mainitun ominaisuuden kehitt�mist� halutaan varmistua siit�, ett� t�lle tuotteelle on my�s taloudellisesti
ajatellen riitt�v�sti kysynt��.
Tehnyt:
Pasi Laakso, 1001087
Sami Ahonen, 0801664
Samir Al Kurdi, 1001046